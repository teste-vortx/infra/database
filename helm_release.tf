provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.name]
      command     = "aws"
    }
  }
}

resource "helm_release" "postgres" {
  name       = "postgres"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql"

  set {
    name  = "postgresqlPassword"
    value = "postgres"
  }
  set {
    name = "postgresqlDatabase"
    value = "todolist_on_herbs_db"
  }
  set {
    name = "service.type"
    value = "LoadBalancer"
  }
}